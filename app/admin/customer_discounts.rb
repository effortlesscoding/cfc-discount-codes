ActiveAdmin.register CustomerDiscount do
  permit_params :code, :redeemed

  controller do
    def new
      super
      @customer_discount.customer = Customer.all.first
    end
  end

  member_action :redeem_code do
    discount_id = params[:discount_id]
    CustomerDiscount.find(discount_id).redeem_code
    redirect_back fallback_location: admin_customer_discount_path(id: discount_id), notice: 'Code redeemed'
  end

  action_item :redeem, only: [:show] do
    link_to 'Redeem', redeem_code_admin_customer_discount_path({ discount_id: customer_discount.id })
  end

end
