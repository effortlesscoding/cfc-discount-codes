class CreateCustomerDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_discounts do |t|
      t.string :code, unique: true
      t.boolean :redeemed, default: false
      t.references :customer, index: true

      t.timestamps
    end
  end
end
