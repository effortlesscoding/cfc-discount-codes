class CustomerDiscount < ApplicationRecord
  after_create :set_code
  belongs_to :customer

  def redeem_code
    self.redeemed = true
    self.save
  end

  def set_code
    return if code
    hashids = Hashids.new("ra#nd1m", 8)
    self.code = hashids.encode(id.to_s)
    unless self.save
      puts 'Errors::'
      puts self.errors
    end
  end
end
