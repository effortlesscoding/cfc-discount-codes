ActiveAdmin.register Customer do
  permit_params :email, :name, :phone
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  index do
    column :name
    column :email
    column :phone
    column :discount_codes do |customer|
      customer.customer_discounts.count
    end
    column :created_at
    actions
  end

  member_action :add_code do
    customer_id = params[:customer_id]
    customer_code = CustomerDiscount.create(customer_id: customer_id)
    redirect_back fallback_location: admin_customer_path(id: customer_id), notice: "Code #{customer_code.code} created"
  end

  action_item :add_code, only: [:show] do
    link_to 'Add discount code', add_code_admin_customer_path({ customer_id: customer.id })
  end

  show do
    attributes_table do
      row :email
      row :phone
      row :name
    end
    panel 'Discount codes' do
      # Show current url
      if customer.customer_discounts.any?
        table_for customer.customer_discounts do
          column :code
          column :redeemed
          column :created_at
          column :view do |discount|
            link_to 'View', admin_customer_discount_path(discount.id)
          end
          column :edit do |discount|
            link_to 'Edit', edit_admin_customer_discount_path(discount.id)
          end
        end
      end
    end
  end
end
