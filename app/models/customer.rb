class Customer < ApplicationRecord
  after_create :create_discount

  has_many :customer_discounts

  def create_discount
    CustomerDiscount.create(customer: self)
  end
end
